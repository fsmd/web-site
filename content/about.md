---
author: Valentin Funk
title: "About / Über"
date: 2020-05-21T13:50:47+02:00
draft: false
description: A short description about the project.
keywords: [about, creator]

---

# About FSMD Project

The **Free Scale Model Development** project brings the values of Open Source 
and Open Knowledge together with scale modelling.

Initiated by true-to-scale passionated model railroaders from the orbit of 
FREMO:87 this may be a place to create, improve and share highly-detailed and cutting-edge 
scale model implementations of vehicles (like trains, trucks and cars), 
buildings (with furnishings) and other thing you would like to have.

The constructions are designed for etching or 3D printing. Done mostly with free 
tools (FreeCAD, Solid Edge 2D, OpenSCAD) and in free file formats (e.g. DXF, 
SVG or scad).

# Über das FSMD-Projekt

Das FSMD-Projekt steht für Free Scale Model Development also **freie Modellbau-Entwicklung**. Frei im Sinne von Open Source und Open Knowledge (Freies Wissen).

Initiiert von zwei Eisenbahn-Modellbauern, die sich dem maßstäblichen Modellbau 
verschrieben haben und aus dem Dunstkreis von FREMO:87 kommen. Dieses Projekt 
soll ein Platz sein, um gemeinsam Modelle von Fahrzeugen (Zügen, LKW, PKW 
u. a.), Gebäuden (samt Einrichtung) und allen anderen Dingen, die Du magst, zu 
erstellen, zu verbessern und zu teilen.

Die Konstruktionen sind entwickelt zum Ätzen oder 3D-Drucken. Bearbeitet mit
kostenlosen Programmen (wie FreeCAD, Solid Edge 2D, OpenSCAD) und meist in 
freien Datei-Formaten (u. a. DXF, SVG, *.scad).
